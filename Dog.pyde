import math
# import math to use math.sqrt to calculate dog and ball velocities (and their components)
import random
# import random to use random.uniform to generate new ball vector components

"""

VARIABLE MAP

+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| NAME                          | TYPE   | PURPOSE                                                                                                                                        |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| bgImg                         | PImage | PImage object for grass.jpg                                                                                                                    |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| dogX                          | float  | x coordinate of dog                                                                                                                            |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| dogY                          | float  | y coordinate of dog                                                                                                                            |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| dogW                          | int    | width of dog                                                                                                                                   |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| dogH                          | int    | height of dog                                                                                                                                  |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| dogImg                        | PImage | PImage object for growlithe.png                                                                                                                |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| ballX                         | float  | x coordinate of ball                                                                                                                           |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| ballY                         | float  | y coordinate of ball                                                                                                                           |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| ballW                         | int    | width of ball                                                                                                                                  |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| ballH                         | int    | height of ball                                                                                                                                 |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| ballImg                       | PImage | PImage object for ball.png                                                                                                                     |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| ballIncrementX                | int    | increment in x direction for ball                                                                                                              |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| ballIncrementY                | int    | increment in y direction for ball                                                                                                              |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| dogIncrementX                 | int    | increment in x direction for dog                                                                                                               |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| dogIncrementY                 | int    | increment in y direction for dog                                                                                                               |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| defaultBallIncrementX         | int    | initial increment in x direction for ball                                                                                                      |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| defaultBallIncrementY         | int    | initial increment in y direction for ball                                                                                                      |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| ballVelocityMagnitude         | float  | magnitude of ball velocity vector                                                                                                              |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| ballRandomness                | float  | maximum randomness factor (for bouncing) allowed                                                                                               |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| dogVelocityIncrement          | float  | increment to magnitude of ball velocity vector                                                                                                 |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| dogMaxVelocityMagnitude       | float  | maximum magnitude of dog's velocity vector; the dog's final velocity (if the dog ever obtains it) will not exceed this value                   |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| dogVelocityMagnitude          | float  | current magnitude of dog's velocity vector                                                                                                     |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| bounceCounter                 | int    | number of times the ball bounces off the border                                                                                                |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| dogVelocityIncrementThreshold | int    | number of times the ball needs the bounce to increase velocity magnitude                                                                       |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| dogVelocityIncrementCounter   | int    | number of times the dog increases velocity magnitude                                                                                           |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+
| time                          | int    | time since the program started; continuously updated to the return value of millis() to animate flashing effect after the dog catches the ball |
+-------------------------------+--------+------------------------------------------------------------------------------------------------------------------------------------------------+

"""

# Load and globalize all images. Initialize and globalize all variables needed for drawing & updating ball & dog positions
def setup():
    global bgImg
    global dogX, dogY, dogW, dogH, dogImg
    global ballX, ballY, ballW, ballH, ballImg
    global ballIncrementX, ballIncrementY, dogIncrementX, dogIncrementY, defaultBallIncrementX, defaultBallIncrementY, ballVelocityMagnitude, ballRandomness
    global dogVelocityIncrement, dogMaxVelocityMagnitude, dogVelocityMagnitude, bounceCounter, dogVelocityIncrementThreshold, dogVelocityIncrementCounter, time
    size(700, 700)
    bgImg = loadImage("grass.jpg")
    dogX = 100
    dogY = 100
    dogW, dogH = 60, 60
    dogImg = loadImage("growlithe.png")
    ballX = 600
    ballY = 100
    ballW, ballH = 40, 40
    ballImg = loadImage("ball.png")
    ballIncrementX = 6
    ballIncrementY = 6
    ballVelocityMagnitude = math.sqrt(ballIncrementX ** 2 + ballIncrementY ** 2)
    defaultBallIncrementX, defaultBallIncrementY = ballIncrementX, ballIncrementY
    dogIncrementX, dogIncrementY = 0, 0
    ballRandomness = 0.8
    dogVelocityMagnitude = ballVelocityMagnitude * 0.1
    dogVelocityIncrement = ballVelocityMagnitude * 0.1
    dogMaxVelocityMagnitude = ballVelocityMagnitude * 0.5
    bounceCounter = 0
    dogVelocityIncrementThreshold = 5
    dogVelocityIncrementCounter = 0
    time = millis()

# Move and draw dog and ball given their positions and detect collision between dog and ball
# If there is collision, do not change dog and ball positions; play flashing animation
def draw():
    global time
    background(bgImg)
    if dogX <= ballX:
        dogImg = loadImage("growlithe.png")
    else:
        dogImg = loadImage("growlithe_flipped.png")
    collided = detectCollision()
    if collided:
        timePassed = millis() - time
        if timePassed >= 400:
            image(dogImg, dogX, dogY, dogW, dogH)
            image(ballImg, ballX, ballY, ballW, ballH)
            if timePassed >= 800:
                time = millis()
    else:
        move()
        adjustBallIncrements()
        adjustDogIncrements()
        image(dogImg, dogX, dogY, dogW, dogH)
        image(ballImg, ballX, ballY, ballW, ballH)

# Increment x and y coordinates of ball and dog
def move():
    global ballX, ballY
    global dogX, dogY
    ballX += ballIncrementX
    ballY += ballIncrementY
    dogX += dogIncrementX
    dogY += dogIncrementY

# Check ball's collision boundaries with walls
# If ball reaches any wall boundary, adjust ball increments by calling getTweakedVectorComponents(); increment bounceCounter
def adjustBallIncrements():
    global ballX, ballY
    global ballIncrementX, ballIncrementY, defaultBallIncrementX, defaultBallIncrementY, ballW, ballH
    global bounceCounter
    # Check left and right bounds collisions
    if ballX >= (700 - ballW) or ballX <= 0:
        ballIncrementX, ballIncrementY = getTweakedVectorComponents(ballIncrementX, ballIncrementY, "x", defaultBallIncrementX, defaultBallIncrementY)
        bounceCounter += 1
    # Check top and bottom bounds collisions
    if ballY >= (700 - ballH) or ballY <= 0:
        ballIncrementX, ballIncrementY = getTweakedVectorComponents(ballIncrementX, ballIncrementY, "y", defaultBallIncrementX, defaultBallIncrementY)
        bounceCounter += 1

# Given the randomness factor, randomly adjust the x and y vector components for the ball's velocity while maintaining the same magnitude
def getTweakedVectorComponents(currentXIncrement, currentYIncrement, axis, defaultXIncrement, defaultYIncrement):
    xIncrementSign = getSign(currentXIncrement)
    yIncrementSign = getSign(currentYIncrement)
    change = random.uniform(-ballRandomness, ballRandomness)
    newXMagnitude = abs(currentXIncrement) * (1 + change)
    while newXMagnitude >= ballVelocityMagnitude:
        newXMagnitude *= 1 - change * 0.2
    newYMagnitude = math.sqrt(ballVelocityMagnitude**2 - newXMagnitude**2)
    if (newXMagnitude*0.2 > newYMagnitude) or (newYMagnitude*0.2 > newXMagnitude):
        newXMagnitude, newYMagnitude = defaultXIncrement, defaultYIncrement
    newXIncrement = newXMagnitude
    newYIncrement = newYMagnitude
    if xIncrementSign == -1:
        newXIncrement *= -1
    if yIncrementSign == -1:
        newYIncrement *= -1
    if axis == "x":
        return newXIncrement * -1, newYIncrement
    else:
        return newXIncrement, newYIncrement * -1

# Adjust dog increments by comparing current dog position and ball position, and current dog velocity
def adjustDogIncrements():
    global ballX, ballY, ballIncrementY, ballIncrementX
    global dogX, dogY, dogIncrementX, dogIncrementY
    global bounceCounter, dogVelocityMagnitude, dogVelocityIncrement, dogMaxVelocityMagnitude, dogVelocityIncrementThreshold, dogVelocityIncrementCounter
    
    if bounceCounter // dogVelocityIncrementThreshold != dogVelocityIncrementCounter and dogVelocityMagnitude < dogMaxVelocityMagnitude:
        dogVelocityMagnitude += dogVelocityIncrement
        dogVelocityIncrementCounter += 1

    xDisplacement = ballX - dogX
    yDisplacement = ballY - dogY
    displacementVectorMagnitude = math.sqrt(yDisplacement**2 + xDisplacement**2)
    ratio = dogVelocityMagnitude / displacementVectorMagnitude
    dogIncrementX = ratio * xDisplacement
    dogIncrementY = ratio * yDisplacement

# Get sign of a integer, returns 1 or -1. 1 and -1 indicate positive and negative numbers respectively
def getSign(num):
    if num >= 0:
        return 1
    else:
        return -1

# Detect collision between ball and dog; returns True if collided; False otherwise
def detectCollision():
    if ballX + ballW >= dogX and ballX <= dogX + dogW and ballY + ballH >= dogY and ballY <= dogY + dogH:
        return True
    else:
        return False
